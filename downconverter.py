#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Downconverter Spurious Products Chart.

References:
    - Mixers of Mircrowave Systems (Part 1), Bert C. Henderson,
        WJ Communications Tech-Note, 2001 (Reprinted)

@auther soerenm
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import tkinter

""" Each pair decribes the coeffecients of one spur for RF and LO.
    E.g. (-2, 4) = -2*R + 4*L """
spurList = [(1, 0), (2, 0), (3, 0),
            (-1, 1), (-2, 1), (-3, 1), (-4, 1), (-5, 1),
            (2, -1), (3, -1), (4, -1), (5, -1),
            (3, -2), (4, -2),
            (4, -3), (6, -5), (5, -4),
            (-3, 2), (-3, 3),
            (-4, 3), (-4, 4),
            (-2, 2), (-5, 4),
            (4, -4), (3, -3), (2, -2), (1, -1),
            (2, -3), (3, -5), (-3, 5), (3, -4),
            (-2, 4), (-1, 2), (-2, 3), (-3, 4)]


def spurPoints(coeff, axisLim, noPoints=100):
    """
    Calculates the mixer spur points.

    Parameters
    ----------
    coeff : array_like
        One coefficient pair (I, L) of the mixer spur chart (see references)
    axisLim : array_like
        Describes the limits of the axis [xmin xmax ymin ymax].
    noPoints : int, optional
        Number of points to calculate.

    Returns
    ----------
    out : tuple (x,y)
        Two array with x and y values of the mixer spur.
    """
    R, L = coeff
    if(R < 0):
        xMin = (axisLim[3]-L)/R
        xMax = (axisLim[2]-L)/R
    else:
        xMin = (axisLim[2]-L)/R
        xMax = (axisLim[3]-L)/R
    if(xMin < 0):
        xMin = 0
    xPoints = np.linspace(xMin, xMax, noPoints)
    return (xPoints, R*xPoints + L)


def sputText(ax):
    """
    Adds text of mixer spurs.

    Parameters
    ----------
    ax :
        Axis of the figure
    """
    ax.text(0.1, 0.095, 'R', rotation=45)
    ax.text(0.107, 0.22, '2R', rotation=63.43)
    ax.text(0.095, 0.29, '3R', rotation=71.57)
    ax.text(0.146, 0.85, 'L-R', rotation=-45)
    ax.text(0.14, 0.705, 'L-2R', rotation=-63.43)
    ax.text(0.136, 0.573, 'L-3R', rotation=-71.56)
    ax.text(0.115, 0.515, 'L-4R', rotation=-75.96)
    ax.text(0.10, 0.464, 'L-5R', rotation=-78.69)
    ax.text(0.597, 0.26, '2R-L', rotation=63.43)
    ax.text(0.52, 0.65, '3R-L', rotation=71.56)
    ax.text(0.425, 0.8, '4R-L', rotation=75.96)
    ax.text(0.335, 0.8, '5R-L', rotation=78.69)
    ax.text(0.66, 0.08, '3R-2L', rotation=71.56)
    ax.text(0.51, 0.15, '4R-2L', rotation=75.96)
    ax.text(0.755, 0.045, '4R-3L', rotation=75.96)
    ax.text(0.835, 0.045, '6R-5L', rotation=80.5)
    ax.text(0.803, 0.045, '5R-4L', rotation=78.69)
    ax.text(0.443, 0.65, '2L-3R', rotation=-71.57)
    ax.text(0.715, 0.83, '3L-3R', rotation=-71.57)
    ax.text(0.53, 0.85, '3L-4R', rotation=-75.96)
    ax.text(0.757, 0.95, '4L-4R', rotation=-75.96)
    ax.text(0.535, 0.92, '2L-2R', rotation=-63.43)
    ax.text(0.625, 0.85, '4L-5R', rotation=-78.69)
    ax.text(1.215, 0.88, '4R-4L', rotation=75.96)
    ax.text(1.265, 0.82, '3R-3L', rotation=71.57)
    ax.text(1.435, 0.9, '2R-2L', rotation=63.43)
    ax.text(1.8, 0.805, 'R-L', rotation=45)
    ax.text(1.822, 0.675, '2R-3L', rotation=63.43)
    ax.text(1.848, 0.58, '3R-5L', rotation=71.57)
    ax.text(1.40, 0.78, '5L-3R', rotation=-71.57)
    ax.text(1.576, 0.76, '3R-4L', rotation=71.57)
    ax.text(1.67, 0.65, '4L-2R', rotation=-63.43)
    ax.text(1.563, 0.43, '2L-R', rotation=-45)
    ax.text(1.253, 0.48, '3L-2R', rotation=-63.43)
    ax.text(1.26, 0.21, '4L-3R', rotation=-71.57)


def plotDownconverterSpurChart(fL, fR):
    """
    Plots mixer spur chart.

    Parameters
    ----------
    fL : numpy array (N)
        LO-Frequency. Unit: [Hz]
    fR: numpy array (Nx2)
        Every row defines the lower and higher frequency of a radio
        frequency band. Unit: [Hz]

    Examples
    ----------
    plotDownconverterSpurChart(np.array([5.2e9, 5.5e9]),
                                        np.array([[5.5e9, 6e9], [6e9, 7e9]]))

    plotDownconverterSpurChart(np.array([7e9]), np.array([[6e9, 7e9]]))

    TODO:
        - Error handling (input, ...)
        - Fuse with upconverter.py ?
    """
    fIF = np.zeros(fR.shape)
    for idx in range(len(fL)):
        # (Normalize to fL)
        if(fR[idx, 1] > fL[idx]):
            fIF[idx, 0] = (fR[idx, 0]-fL[idx])/fL[idx]
            fIF[idx, 1] = (fR[idx, 1]-fL[idx])/fL[idx]
        else:
            fIF[idx, 0] = (fL[idx]-fR[idx, 1])/fL[idx]
            fIF[idx, 1] = (fL[idx]-fR[idx, 0])/fL[idx]
        fR[idx, 0] = fR[idx, 0]/fL[idx]
        fR[idx, 1] = fR[idx, 1]/fL[idx]

    axisLim = np.array([0.0, 2.0, 0.0, 1.0])
    ''' Uncomment if axisLim are to small -> Looks (most likely) deformed '''
    # if(fR_high > 2.0):
    #     axisLim[1] = fR_high + round(0.25*fR_high, 1)
    # if(fIF_high > 1.0):
    #     axisLim[3] = fIF_high + round(0.25*fIF_high, 1)
    stepSize = 0.1
    xTicks = np.arange(axisLim[0], axisLim[1]+stepSize, stepSize)
    yTicks = np.arange(axisLim[2], axisLim[3]+stepSize, stepSize)

    fig, ax = plt.subplots()
    fig.suptitle('Downconverter Spurious Products')
    ax.set_xlabel('$f_R/f_L$')
    ax.set_ylabel('$f_{IF}/f_L$')
    for spur in spurList:
        a, b = spurPoints(spur, axisLim)
        ax.plot(a, b, 'tab:blue', linewidth=1)
    sputText(ax)
    colorList = ['tab:red', 'tab:green', 'tab:orange', 'tab:purple']
    for idx in range(len(fL)):
        labelRec = '$f_L$ = ' + '{0:.1f}'.format(fL[idx]/1e6) + \
              ' MHz, $f_R$ = ' + ' {0:.1f}'.format(fR[idx, 0]*fL[idx]/1e6) + \
              ' - ' + '{0:.1f}'.format(fR[idx, 1]*fL[idx]/1e6) + ' MHz'
        ax.add_patch(Rectangle((fR[idx, 0], fIF[idx, 0]), fR[idx, 1] -
                               fR[idx, 0], fIF[idx, 1]-fIF[idx, 0],
                               0.0, edgecolor=colorList[idx % len(colorList)],
                               fill=False, linewidth=1.5, label=labelRec))
    ax.set_xlim([axisLim[0], axisLim[1]])
    ax.set_ylim([axisLim[2], axisLim[3]])
    ax.set_xticks(xTicks)
    ax.set_yticks(yTicks)
    if(fR[0, 1] > 1):
        ax.legend(loc='center left')
    else:
        ax.legend(loc='center right')
    # Figure to fullscreen
    DPI = fig.get_dpi()
    tk = tkinter.Tk()
    width = tk.winfo_screenwidth()
    height = tk.winfo_screenheight()
    fig.set_size_inches(width/float(DPI), height/float(DPI))
    plt.show()


if __name__ == "__main__":
    plotDownconverterSpurChart(np.array([5.2e9, 5.5e9]),
                               np.array([[5.5e9, 6e9], [6e9, 7e9]]))
