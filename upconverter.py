#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Upconverter Spurious Products Chart.

References:
    - Mixers of Mircrowave Systems (Part 1), Bert C. Henderson,
        WJ Communications Tech-Note, 2001 (Reprinted)

@auther soerenm
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import tkinter

""" Each pair decribes the coeffecients of one spur for IF and LO (I, L).
    E.g. (-1, 2) = -1*I + 2*L """
spurList = [(1, 0), (2, 0), (3, 0), (4, 0),
            (-1, 2), (-2, 2), (-3, 2), (-4, 2),
            (-1, 1), (-3, 3), (-4, 4),
            (3, -1), (4, -1), (4, -2), (5, -1),
            # Only Upper Sideband Output lines
            (1, 1), (2, 1), (3, 1),
            (-2, 3), (-3, 4), (-4, 5),
            # Only Lower Sideband Output lines
            (-2, 1), (-3, 1), (-4, 1), (-5, 1),
            (2, -1), (3, -2)]


def spurPoints(coeff, axisLim, noPoints=2):
    """
    Calculates the mixer spur points.

    Parameters
    ----------
    coeff : array_like
        One coefficient pair (I, L) of the mixer spur chart (see references)
    axisLim : array_like
        Describes the limits of the axis [xmin xmax ymin ymax].
    noPoints : int, optional
        Number of points to calculate.

    Returns
    ----------
    out : tuple (x,y)
        Two array with x and y values of the mixer spur.
    """
    I, L = coeff
    if(I < 0):
        xMin = (axisLim[3]-L)/I
        xMax = (axisLim[2]-L)/I
    else:
        xMin = (axisLim[2]-L)/I
        xMax = (axisLim[3]-L)/I
    if(xMin < 0):
        xMin = 0
    if(xMax > 1):
        xMax = 1
    # print("({}, {}); xMin = {}, xMax = {}".format(I,L,xMin,xMax))
    xPoints = np.linspace(xMin, xMax, noPoints)
    return (xPoints, I*xPoints + L)


def sputText(ax):
    """
    Adds text of mixer spurs.

    Parameters
    ----------
    ax :
        Axis of the figure
    """
    ax.text(0.623, 0.633, 'I', rotation=15.41)
    ax.text(0.28, 0.59, '2I', rotation=28.87)
    ax.text(0.7, 1.43, '2I', rotation=28.87)
    ax.text(0.21, 0.67, '3I', rotation=39.62)
    ax.text(0.626, 1.92, '3I', rotation=39.62)
    ax.text(0.17, 0.73, '4I', rotation=47.8)
    ax.text(0.36, 1.49, '4I', rotation=47.8)
    ax.text(0.13, 1.88, '2L-I', rotation=-15.41)
    ax.text(0.1, 1.802, '2L-2I', rotation=-28.87)
    ax.text(0.1, 1.7, '2L-3I', rotation=-39.62)
    ax.text(0.176, 1.29, '2L-4I', rotation=-47.8)
    ax.text(0.83, 0.34, '2L-2I', rotation=-28.87)
    ax.text(0.62, 0.14, '2L-3I', rotation=-39.62)
    ax.text(0.45, 0.19, '2L-4I', rotation=-47.8)
    ax.text(0.84, 0.17, 'L-I', rotation=-15.41)
    ax.text(0.85, 0.45, '3L-3I', rotation=-39.62)
    ax.text(0.38, 1.855, '3L-3I', rotation=-39.62)
    ax.text(0.87, 0.51, '4L-4I', rotation=-47.8)
    ax.text(0.64, 1.43, '4L-4I', rotation=-47.8)
    ax.text(0.52, 0.57, '3I-L', rotation=39.62)
    ax.text(0.86, 1.59, '3I-L', rotation=39.62)
    ax.text(0.30, 0.21, '4I-L', rotation=47.8)
    ax.text(0.68, 1.735, '4I-L', rotation=47.8)
    ax.text(0.61, 0.525, '4I-2L', rotation=47.8)
    ax.text(0.81, 1.27, '4I-2L', rotation=47.8)
    ax.text(0.57, 1.93, '5I-L', rotation=54.04)
    ax.text(0.26, 0.32, '5I-L', rotation=54.04)
    ax.text(0.81, 1.835, 'L+I', rotation=15.41)
    ax.text(0.41, 1.83, 'L+2I', rotation=28.87)
    ax.text(0.26, 1.80, 'L+3I', rotation=39.62)
    ax.text(0.7, 1.60, '3L-2I', rotation=-28.87)
    ax.text(0.78, 1.655, '4L-3I', rotation=-39.62)
    ax.text(0.91, 1.35, '5L-4I', rotation=-47.8)
    ax.text(0.36, 0.278, 'L-2I', rotation=-28.87)
    ax.text(0.31, 0.065, 'L-3I', rotation=-39.62)
    ax.text(0.18, 0.27, 'L-4I', rotation=-47.8)
    ax.text(0.15, 0.24, 'L-5I', rotation=-54.04)
    ax.text(0.68, 0.405, '2I-L', rotation=28.87)
    ax.text(0.76, 0.3, '3I-2L', rotation=39.62)


def plotRec(ax, color, f_IF_low, f_IF_high, f_RF_low, f_RF_high, fL):
    """
    Plots the rectangle to determine mixer spurs.

    Parameters
    ----------
    ax :
        Axis of the figure
    color: mpl color spec
        Color of the rectangle.
    f_IF_low: double
        Low intermediate frequency normalised to LO-Frequency.
    f_IF_high: double
        High intermediate frequency normalised to LO-Frequency.
    f_RF_low: double
        Low radio frequency normalised to LO-Frequency.
    f_RF_high: double
        High radio frequency normalised to LO-Frequency.
    fL: double
        LO-Frequency in [Hz]
    """
    labelRec = '$f_L$ = ' + '{0:.1f}'.format(fL/1e6) + \
               ' MHz, $f_R$ = ' + ' {0:.1f}'.format(f_RF_low*fL/1e6) + \
               ' - ' + '{0:.1f}'.format(f_RF_high*fL/1e6) + ' MHz'
    ax.add_patch(Rectangle((f_IF_low, f_RF_low), f_RF_high-f_RF_low,
                           f_IF_high-f_IF_low, 0.0, edgecolor=color,
                           fill=False, linewidth=1.5, label=labelRec))


def plotUpconverterSpurChart(fL, f_IF, indicator=None):
    """
    Plots mixer spur chart.

    Parameters
    ----------
    fL : double or numpy array
        LO-Frequency. If the parameter is a single value, will be used
        for every intermediate frequency band.  Unit: [Hz]
    f_IF: numpy array (Nx2)
        Every row defines the lower and higher frequency of an intermediate
        frequency band. Unit: [Hz]
    indicator: numpy array, optional
        Define sidebands (lower/upper)
        0 lower sideband
        1 upper sideband
        2 lower and upper sideband (default)
        If it's used, has to be the same number of rows as f_IF

    Example
    ----------
    plotUpconverterSpurChart(np.array([30e9]), np.array([[2e9, 4e9]]),
                                      np.array([1]))

    TODO:
        - Error handling (input, ...)
        - Fuse with downconverter.py ?
    """
    if(indicator is None):
        # defautl: lower and upper sideband
        indicator = np.ones(f_IF.shape[0]) * 2

    if(fL.shape[0] != f_IF.shape[0]):
        fL = np.ones(f_IF.shape[0]) * fL

    noRect = np.sum(indicator)
    for i in range(len(indicator)):
        if(indicator[i] == 0):
            noRect = noRect + 1
    axisLim = np.array([0.0, 1.0, 0.0, 2.0])
    stepSize = 0.1
    xTicks = np.arange(axisLim[0], axisLim[1]+stepSize, stepSize)
    yTicks = np.arange(axisLim[2], axisLim[3]+stepSize, stepSize)

    fig, ax = plt.subplots()
    fig.suptitle('Upconverter Spurious Products')
    ax.set_xlabel('IF Input $f_{In}/f_L$')
    ax.set_ylabel('Lower [0:1] and Upper [1:2] Sideband Output $f_{Out}/f_L$')
    for spur in spurList:
        a, b = spurPoints(spur, axisLim)
        ax.plot(a, b, 'tab:blue', linewidth=1)
    sputText(ax)
    colorList = ['tab:red', 'tab:green', 'tab:orange', 'tab:purple']

    idxRF = 0
    for idx in range(fL.shape[0]):
        if(indicator[idx] == 0):
            f_IF_low = f_IF[idx, 0]/fL[idx]
            f_IF_high = f_IF[idx, 1]/fL[idx]
            f_RF_low = (fL[idx] - f_IF[idx, 1])/fL[idx]
            f_RF_high = (fL[idx] - f_IF[idx, 0])/fL[idx]
            plotRec(ax, colorList[idx % len(colorList)], f_IF_low, f_IF_high,
                    f_RF_low, f_RF_high, fL[idx])
            idxRF = idxRF + 1
        elif(indicator[idx] == 1):
            f_IF_low = f_IF[idx, 0]/fL[idx]
            f_IF_high = f_IF[idx, 1]/fL[idx]
            f_RF_low = (fL[idx] + f_IF[idx, 0])/fL[idx]
            f_RF_high = (fL[idx] + f_IF[idx, 1])/fL[idx]
            plotRec(ax, colorList[idx % len(colorList)], f_IF_low, f_IF_high,
                    f_RF_low, f_RF_high, fL[idx])
            idxRF = idxRF + 1
        elif(indicator[idx] == 2):
            f_IF_low = f_IF[idx, 0]/fL[idx]
            f_IF_high = f_IF[idx, 1]/fL[idx]
            f_RF_low = (fL[idx] - f_IF[idx, 1])/fL[idx]
            f_RF_high = (fL[idx] - f_IF[idx, 0])/fL[idx]
            plotRec(ax, colorList[idx % len(colorList)], f_IF_low, f_IF_high,
                    f_RF_low, f_RF_high, fL[idx])
            idxRF = idxRF + 1
            f_IF_low = f_IF[idx, 0]/fL[idx]
            f_IF_high = f_IF[idx, 1]/fL[idx]
            f_RF_low = (fL[idx] + f_IF[idx, 0])/fL[idx]
            f_RF_high = (fL[idx] + f_IF[idx, 1])/fL[idx]
            plotRec(ax, colorList[idx % len(colorList)], f_IF_low, f_IF_high,
                    f_RF_low, f_RF_high, fL[idx])
            idxRF = idxRF + 1

    ax.set_xlim([axisLim[0], axisLim[1]])
    ax.set_ylim([axisLim[2], axisLim[3]])
    ax.set_xticks(xTicks)
    ax.set_yticks(yTicks)
    ax.legend(loc='best')
    # Figure to fullscreen
    DPI = fig.get_dpi()
    tk = tkinter.Tk()
    width = tk.winfo_screenwidth()
    height = tk.winfo_screenheight()
    fig.set_size_inches(width/float(DPI), height/float(DPI))
    plt.show()


if __name__ == "__main__":
    plotUpconverterSpurChart(np.array([30e9, 5e9, 8e9]), np.array([[2e9, 4e9],
                             [4.5e9, 4.8e9], [200e6, 400e6]]),
                             np.array([2, 1, 0]))
